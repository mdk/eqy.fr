# Generated by Django 5.0 on 2023-12-20 16:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("photos", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="media",
            name="title",
            field=models.CharField(default="Sans titre", max_length=500),
            preserve_default=False,
        ),
    ]
