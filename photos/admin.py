from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from photos.models import Media

@admin.register(Media)
class MediaAdmin(TranslationAdmin):
    ...
