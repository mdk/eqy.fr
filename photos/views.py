from django.shortcuts import render
from django.utils.translation import gettext as _

from photos.models import Media


def index(request):
    title = _("Title")
    return render(request, "photos/index.html", context={"title": title, "medias": Media.objects.all()})
