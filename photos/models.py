from django.db import models


class Media(models.Model):
    title = models.CharField(max_length=500)
    photo = models.FileField(upload_to="photos/%Y/", null=True, blank=True)
    video = models.FileField(upload_to="videos/%Y/", null=True, blank=True)

    def __str__(self):
        if self.photo:
            return self.photo.name
        elif self.video:
            return self.video.name
        return super().__str__()
