from modeltranslation.translator import TranslationOptions, translator

from photos.models import Media


class MediaTranslationOptions(TranslationOptions):
    fields = ("title",)


translator.register(Media, MediaTranslationOptions)
