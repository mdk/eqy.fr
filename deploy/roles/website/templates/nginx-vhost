server {
    listen [::]:80;
    listen 80;
    server_name {{ website_vhost }};

    location / {
        return 301 https://{{ website_vhost }}$request_uri;
    }
}

server {
    listen 443 http2 ssl;
    listen [::]:443 http2 ssl;
    server_name {{ website_vhost }};

    client_max_body_size 666M;

    include snippets/letsencrypt-{{ cert_name }}.conf;

    location /static/ {
        alias /opt/website/static/;
    }

    location /media/ {
        alias /opt/website/media/;
    }

    location / {
        proxy_pass http://unix:/opt/website/website.sock;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-Protocol $scheme;
    }
}
